<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02 - Activity</title>
</head>
<body>

<!-- <h1>Test</h1> -->

	<!-- ITEM 1 -->
	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		
		<h1>Divisibles of Five</h1>
		<?php divisibleByFive() ?>

	</div>

	<div style="padding: 10px; border: none; width: auto; display: block;"></div>

	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		<h1>Array Manipulation</h1>
		
		<?php 
			array_push($students, "John Smith");
			var_dump($students);
		?>
		<div>
			<?php 
				echo count($students);
			?>
		</div>

		<?php 
			array_push($students, "Jane Smith");
			var_dump($students);
		?>
		<div>
			<?php 
				echo count($students);
			?>
		</div>

		<?php 
			array_shift($students);
			var_dump($students);
		?>
		<div>
			<?php 
				echo count($students);
			?>
		</div>

	</div>

</body>
</html>